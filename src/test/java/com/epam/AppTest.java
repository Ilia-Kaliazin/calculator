package com.epam;

import com.epam.library.TestExpression;
import com.epam.library.TestSymmetry;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestSymmetry.class,
        TestExpression.class
})

public class AppTest 
{
}
