package com.epam.library;

import com.epam.calculator.Core;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSymmetry {
    public Core core = new Core();

    @Test
    public void checkEquals123Plus321And321Plus123 (){
        core.run("123 + 321");
        double result1 = core.getResult();
        core.run("321 + 123");
        assertEquals(result1, core.getResult(), 0);
    }

    @Test
    public void checkEquals123Mult321And321Mult123 (){
        core.run("123 * 321");
        double result1 = core.getResult();
        core.run("321 * 123");
        assertEquals(result1, core.getResult(), 0);
    }

    @Test
    public void checkEquals10Minus20And20Minus10MultOnMinus1() {
        core.run("10-20");
        double result1 = core.getResult();
        core.run("20 - 10");
        assertEquals(result1, core.getResult()*(-1), 0.0);
    }
}
