package com.epam.library;

import com.epam.calculator.Core;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestExpression {
    public Core core = new Core();

    @Test (expected = NumberFormatException.class)
    public void checkExceptionExpressionNumberFract0(){
        core.run("123421  /   0 ");
    }

    @Test
    public void checkExceptionExpressionNumberFractFractNumber(){
        core.run("123421  / /  123 ");
    }
    @Test
    public void checkExceptionExpressionNumberMultMultNumber(){
        core.run("123421  * * 132 ");
    }

    @Test
    public void checkResultMinusNumber(){
        core.run("-123");
        assertEquals(-123, core.getResult(), 0.0);
    }

    @Test
    public void checkResultSqrt81(){
        core.run("sqrt81");
        assertEquals(Math.sqrt(81), core.getResult(), 0.0);
    }

    @Test
    public void checkResultSqrtNumber(){
        core.run("sqrt9.4");
        assertEquals(Math.sqrt(9.4), core.getResult(), 0.0);
    }

    @Test
    public void checkResultMultNumber(){
        core.run("9 * 9");
        assertEquals(9 * 9, core.getResult(), 0.0);
    }

    @Test
    public void checkResultMultDoubleNumber(){
        core.run("54.31 * 21.51 * 31.13");
        assertEquals(54.31 * 21.51 * 31.13, core.getResult(), 0.0);
    }

    @Test
    public void checkResultFractNumber(){
        core.run("9/3 ");
        assertEquals( (double) 9 / 3, core.getResult(), 0.0);
    }

    @Test
    public void checkResultFractDoubleNumber(){
        core.run("54.31 / 24.51 / 31.13");
        assertEquals(54.31 / 24.51 / 31.13, core.getResult(), 0.0);
    }

    @Test
    public void checkResultFractDoubleMixedNumber(){
        core.run("8 * 54.3 + 31.0 / 51.0 + sqrt4 * 54.0");
        assertEquals(8 * 54.3 + 31.0 / 51.0 + Math.sqrt(4) * 54.0, core.getResult(), 0.0);
    }

    @Test
    public void checkResultDoubleNumber(){
        core.run("54.31 / 231.213 * 24.51 - 0.12321 *31.13");
        assertEquals(54.31 / 231.213 * 24.51 - 0.12321 * 31.13, core.getResult(), 0.0);
    }
}
