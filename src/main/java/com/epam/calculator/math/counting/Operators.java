package com.epam.calculator.math.counting;

public class Operators {

    public static String summ (String string1, String string2) {
        double number1 = Double.parseDouble(string1);
        double number2 = Double.parseDouble(string2);
        return String.valueOf(number1 + number2);
    }

    public static String minus (String string1, String string2) {
        double number1 = Double.parseDouble(string1);
        double number2 = Double.parseDouble(string2);
        return String.valueOf(number1 - number2);
    }

    public static String fract (String string1, String string2) {
        double number1 = Double.parseDouble(string1);
        double number2 = Double.parseDouble(string2);
        return String.valueOf(number1 / number2);
    }

    public static String mult (String string1, String string2) {
        double number1 = Double.parseDouble(string1);
        double number2 = Double.parseDouble(string2);
        return String.valueOf(number1 * number2);
    }

    public static String sqrt (String string) {
        double number = Double.parseDouble(string);
        if (number < 0) throw new ArithmeticException();
        return String.valueOf(Math.sqrt(number));
    }
}
