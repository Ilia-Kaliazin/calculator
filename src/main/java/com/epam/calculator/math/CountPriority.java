package com.epam.calculator.math;

public class CountPriority {
    /**
     * @param string - входная строка.
     * @return уровень приоритета.
     */
    public static int getLvlMaxPriority (String string){
        if(string.contains("sqrt")) return 5;
        if(string.contains("/")) return 4;
        if(string.contains("*")) return 3;
        if(string.contains("-") && string.indexOf("-") != 0) return 1;
        if(string.contains("+")) return 2;
        return 0;
    }

    /**
     * @param string - входная строка.
     * @return позиция оператора максимального уровня приоритета.
     */
    public static int getPositionMaxPriorityOperator (String string){
        if(string.contains("sqrt")) return string.indexOf("sqrt");
        if(string.contains("/")) return string.indexOf("/");
        if(string.contains("*")) return string.indexOf("*");
        if(string.contains("-") && string.indexOf("-") != 0) return string.indexOf("-");
        if(string.contains("+")) return string.indexOf("+");
        return -1;
    }
}
