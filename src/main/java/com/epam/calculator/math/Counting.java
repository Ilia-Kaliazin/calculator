package com.epam.calculator.math;

import com.epam.calculator.Core;
import com.epam.calculator.math.counting.Operators;
import com.epam.calculator.redactor.Delimiter;

public class Counting {
    private Core core;
    private String value;
    private String[] arrayString;
    private static Counting instance;

    private Counting(Core core) {
        this.core = core;
        this.arrayString = new String[2];
    }

    public static Counting getInstance(Core core) {
        if (instance == null) return new Counting(core);
        return instance;
    }

    /**
     * Получение и запись в массив подвыражения из строки по указателю.
     * @param positionOperator - указатель на оператор в строке.
     */
    private void splitValueOnNumber(int positionOperator){
        if(value.contains("sqrt")){
            arrayString[0] = value.substring(positionOperator + 4, Delimiter.getNextOperator(value, positionOperator));
            return;
        }
        arrayString[0] = value.substring(Delimiter.getBeforeOperator(value, positionOperator), positionOperator);
        arrayString[1] = value.substring(positionOperator + 1, Delimiter.getNextOperator(value, positionOperator));
    }

    /**
     * Вычисление подвыражения.
     */
    public void run() {
        try {
            splitValueOnNumber(CountPriority.getPositionMaxPriorityOperator(value));

            switch (CountPriority.getLvlMaxPriority(value)) {
                case 0:
                    return;
                case 1:
                    core.replaceExpressionOnNumber(Operators.minus(arrayString[0], arrayString[1]));
                    break;
                case 2:
                    core.replaceExpressionOnNumber(Operators.summ(arrayString[0], arrayString[1]));
                    break;
                case 3:
                    core.replaceExpressionOnNumber(Operators.mult(arrayString[0], arrayString[1]));
                    break;
                case 4:
                    core.replaceExpressionOnNumber(Operators.fract(arrayString[0], arrayString[1]));
                    break;
                case 5:
                    core.replaceExpressionOnNumber(Operators.sqrt(arrayString[0]));
                    break;
            }
        } catch (NumberFormatException e){
            core.onException(e);
        }
    }

    public void setValue(String value) {
        this.value = value;
    }
}
