package com.epam.calculator.redactor;

public class Delimiter {
    /**
     * @param string - входная строка.
     * @param i - указатель на начальный оператор.
     * @return - позиция предыдущего оператора относительно указателя.
     */
    public static int getBeforeOperator (String string, int i) {
        int j = i ;
        if (i > 0) j = i - 1;

        while (j != 0){
            switch (string.charAt(j)) {
                case '+': return j + 1;
                case '-': return j + 1;
                case '*': return j + 1;
                case '/': return j + 1;
                case 's': return j + 1;
            }
            j--;
        }
        return j;
    }

    /**
     * @param string - входная строка.
     * @param i - указатель на начальный оператор.
     * @return - позиция следующего оператора относительно указателя.
     */
    public static int getNextOperator (String string, int i) {
        int j = i + 1;
        while (j != string.length()){
            switch (string.charAt(j)) {
                case '+': return j;
                case '-': return j;
                case '*': return j;
                case '/': return j;
                case 's': return j;
            }
            j++;
        }
        return j;
    }

    /**
     * @param string - входная строка.
     * @param i - указатель на начальный оператор.
     * @return - выражение содержащее два числа и оператор.
     */
    public static String getExpression (String string, int i) {
        return string.substring(getBeforeOperator(string, i), getNextOperator(string, i));
    }
}
