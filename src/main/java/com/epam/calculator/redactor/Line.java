package com.epam.calculator.redactor;

public class Line {
    private static Line instance;
    private Line(){}

    public static Line getInstance(){
        if(instance == null) return new Line();
        return instance;
    }

    public String removeSpace (String string) {
        return string.replace(" ", "");
    }

    /**
     * @param string - входная строка.
     * @param number - результат подвыражения.
     * @param i - указатель на оператор, который будем заменять.
     * @return Выражение с заменённым подвыражением на число.
     */
    public String replaceCountWhitNumber (String string, String number, int i){
        return string.replace(Delimiter.getExpression(string, i), number);
    }
}
