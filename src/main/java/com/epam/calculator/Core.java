package com.epam.calculator;

import com.epam.calculator.console.Communication;
import com.epam.calculator.math.CountPriority;
import com.epam.calculator.math.Counting;
import com.epam.calculator.redactor.Line;
import com.epam.calculator.redactor.Delimiter;

public class Core {
    Line line;
    Counting counting;
    Communication communication;
    ValidateValue validateValue;

    private String string;
    private int pointerToOperator;

    public Core () {
        this.line = Line.getInstance();
        this.counting = Counting.getInstance(this);
        this.validateValue = ValidateValue.getInstance();
        this.communication = Communication.getInstance(this);
    }

    public void run (String string){
        if(string == null) {
            communication.run();
            return;
        }
        this.string = line.removeSpace(string);

        validateValue.run(this.string);
        setValueExpression();
    }

    /**
     * Вычисление выражения.
     * Если в выражении есть оператор, тогда вычисляем..
     * Иначе выводим ответ.
     */
    private void setValueExpression () {
        if (CountPriority.getLvlMaxPriority(string) > 0) count(CountPriority.getPositionMaxPriorityOperator(string));
        else communication.printToUser(String.format("Ответ: %s\n", getResult()));
    }

    /**
     * @param positionOperator - порядковое число оператора в выражении.
     */
    private void count(int positionOperator){
        pointerToOperator = positionOperator;
        counting.setValue(Delimiter.getExpression(string, positionOperator));
        counting.run();
    }

    /**
     * @param number - результат вычисления подвыражения.
     */
    public void replaceExpressionOnNumber (String number) {
        string = line.replaceCountWhitNumber(string , number, pointerToOperator);
        setValueExpression();
    }

    public double getResult () {
        if(string != null) return Double.parseDouble(string);
        throw new ArithmeticException("Incorrect expression.");
    }

    public void onException(Exception e){
        string = null;
        communication.printToUser("Incorrect expression, " + e.getClass() + "\n");
    }
}
