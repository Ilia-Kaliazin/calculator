package com.epam.calculator;

import com.epam.calculator.redactor.Delimiter;

public class ValidateValue {
    private String string;
    private static ValidateValue instance;

    public static ValidateValue getInstance() {
        if (instance == null) return new ValidateValue();
        return instance;
    }

    /**
     * Проверка на существование корня.
     * Проверка числа после sqrt отличного от 0.
     */
    private void checkSQRT() {
        if (!string.toLowerCase().contains("sqrt")) return;
        if (Double.parseDouble(String.valueOf(string.charAt(string.indexOf("sqrt")) + 1)) > 0) return;
        throw new NumberFormatException("Incorrect sqrt.");
    }


    /**
     * Проверка на существование деления.
     * Проверка числа в знаменателе.
     */
    private void checkFractZero() {
        if (!string.toLowerCase().contains("/")) return;
        String number = string.substring(string.indexOf("/") + 1, Delimiter.getNextOperator(string, string.indexOf("/")));
        if (!number.equals("0")) return;
        throw new NumberFormatException("Not allowed / Zero.");
    }

    private boolean totalValidate() {
        checkSQRT();
        checkFractZero();
        return true;
    }

    public void run(String string) {
        this.string = string;
        totalValidate();
    }
}
