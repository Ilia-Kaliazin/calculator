package com.epam.calculator.console;

import com.epam.calculator.Core;

import java.util.Scanner;

public class Communication {
    private Core core;
    private Scanner scanner;
    private static Communication instance;

    private Communication (Core core){
        this.core = core;
        this.scanner = new Scanner(System.in);
    }

    public static Communication getInstance(Core core){
        if(instance == null) return new Communication(core);
        return instance;
    }

    private void requestExpressionAtUser (){
        printToUser("Give me expression: ");
        core.run(inputValue(null));
    }


    public String inputValue (String string) {
        return (string == null) ? scanner.next() : string;
    }

    public void printToUser (String string) {
        System.out.print(string);
    }

    public void run (){
        requestExpressionAtUser();
    }
}

